import Vue from "vue";
import VueRouter from "vue-router";
import CalendarPage from "../pages/CalendarPage.vue";
Vue.use(VueRouter);

const routes = [
  {
    path: "/calendar",
    name: "Calendar",
    component: CalendarPage,
  },
];

const router = new VueRouter({
  routes,
  mode: "history",
});

export default router;
